import { EventEmitter, Injectable } from '@angular/core';
import { NavigationStart, Router, RouterEvent } from '@angular/router';
import GraphicsLayer from 'arcgis-js-api/layers/GraphicsLayer';
import { FeatureCollection } from 'geojson';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { HeatmapSettings } from '@heatmap/interfaces/heatmap-settings';
import { LayerElement } from '@map/interfaces/layer-element';
import { Store } from '@store/interfaces/store';
import { Trail } from '@map/interfaces/trail';
import { ViewSettings } from '@map/interfaces/view-settings';

import { AuthorizationService } from '@access/authorization/services/authorization.service';
import { DataService } from '@data/services/data.service';
import { EventEmitterService } from '@core/services/eventEmitter/event-emitter.service';
import { GraphicsLayerService } from '@map/services/graphicsLayer/graphics-layer.service';
import { HeatmapService } from '@heatmap/services/heatmap.service';
import { LayerUIService } from '@map/services/layerUI/layer-ui.service';
import { MapService } from '@map/services/map.service';
import { StoreService } from '@store/services/store.service';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {
  private eventEmitter: EventEmitter<any>;
  private state: Store;

  constructor(
    private router: Router,
    private authorizationService: AuthorizationService,
    private dataService: DataService,
    private eventEmitterService: EventEmitterService,
    private graphicsLayerService: GraphicsLayerService,
    private heatmapService: HeatmapService,
    private layerUIService: LayerUIService,
    private mapService: MapService,
    private storeService: StoreService
  ) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
  }

  setSubscriptions(): void {
    const setEvents: Subscription = this.eventEmitter.subscribe({
      next: (params: any[]): void => {
        const event: string = params[0];

        switch (event) {
          case 'addGraphicsLayer': {
            {
              const id: string = params[1];
              const gl: GraphicsLayer = this.graphicsLayerService.graphicsLayers[
                this.graphicsLayerService.graphicsLayersHash[id]
              ];

              this.mapService.addGraphicsLayer(gl);
            }
            break;
          }

          case 'addGraphicsLayers':
            this.graphicsLayerService.graphicsLayers
              .filter((gl: GraphicsLayer): boolean => gl.visible)
              .forEach((gl: GraphicsLayer): void => {
                setTimeout((): void => this.mapService.addGraphicsLayer(gl), 500);
              });

            setTimeout((): void => this.eventEmitter.emit(['setSplashScreenActive']), 1000);
            break;

          case 'createGraphicsLayer':
            {
              const fc: FeatureCollection = params[1];
              const id: string = params[2];

              this.graphicsLayerService.createGraphicsLayer(fc, id);
            }
            break;

          case 'loadData':
            this.dataService.loadData();
            break;

          case 'loadHeatmap':
            this.heatmapService.loadMap();
            break;

          case 'loadMap':
            this.mapService.loadMap();
            break;

          case 'logout':
            this.authorizationService.unsetSession();
            break;

          case 'removeGraphicsLayer': {
            {
              const id: string = params[1];
              const gl: GraphicsLayer = this.graphicsLayerService.graphicsLayers[
                this.graphicsLayerService.graphicsLayersHash[id]
              ];

              this.mapService.removeGraphicsLayer(gl);
            }
            break;
          }

          case 'resetHeatmapParams':
            this.heatmapService.resetParams();
            break;

          case 'resetHeatmapSettings':
            this.storeService.resetHeatmapSettings();
            this.heatmapService.resetSettings();
            break;

          case 'selectLayer':
            {
              const id: string = params[1];

              this.layerUIService.selectLayer(id);
              this.eventEmitter.emit(['setLayerElementActive', id]);
            }
            break;

          case 'selectTrail':
            {
              const name: string = params[1];
              const i: number = this.state.trails.findIndex((trail: Trail): boolean => trail.name === name);

              this.mapService.goTo(this.state.trails[i]);
              this.storeService.setTrailActive(i);
            }
            break;

          case 'setBasemapActive':
            this.storeService.setBasemapActive();
            break;

          case 'setGraphicsLayerActive':
            {
              const id: string = params[1];

              this.graphicsLayerService.setGraphicsLayerVisible(id);
              this.storeService.setGraphicsLayerActive(id);
            }
            break;

          case 'setHeatmapActive':
            this.storeService.setHeatmapActive();
            break;

          case 'setHeatmapData':
            const data: any[] = params[1];

            this.heatmapService.data = data;
            break;

          case 'setHeatmapParams':
            const key: string = params[1];
            const value: number = params[2];

            this.storeService.setHeatmapParams(key, value);
            break;

          case 'setHeatmapSettings':
            const heatmapSettings: HeatmapSettings = params[1];

            this.storeService.setHeatmapSettings(heatmapSettings);
            break;

          case 'setLayerElementActive':
            {
              const id: string = params[1];
              const i: number = this.state.layerElements.findIndex((el: LayerElement): boolean => el.id === id);

              switch (id) {
                case 'biosphere':
                case 'office':
                case 'places':
                case 'trails':
                  this.storeService.setLayerElementActive(i);
                  break;
              }
            }
            break;

          case 'setMapActive':
            this.storeService.setMapActive();
            break;

          case 'setRoutePath':
            const routePath: string = params[1];

            this.router.navigate([routePath]);
            break;

          case 'setSplashScreenActive':
            this.storeService.setSplashScreenActive();
            break;

          case 'setViewSettings':
            const viewSettings: ViewSettings = params[1];

            this.storeService.setViewSettings(viewSettings);
            break;
        }
      },
      error: (err: Error): void => {
        console.error('setEvents Failed:\n', err);
      },
      complete: (): void => {
        setEvents.unsubscribe();
      }
    });

    const setRoutes: Subscription = this.router.events
      .pipe(
        filter((evt: RouterEvent): boolean => {
          return evt instanceof NavigationStart;
        })
      )
      .subscribe({
        next: (evt: NavigationStart): any => {
          if (!this.authorizationService.hasSessionExpired() && evt.url !== '/') {
            if (!this.state.splashScreen.active) {
              this.eventEmitter.emit(['setSplashScreenActive']);
            }

            if (evt.url === '/map') {
              if (this.state.heatmap.active) {
                this.eventEmitter.emit(['setHeatmapActive']);
              }

              if (!this.state.map.active) {
                this.eventEmitter.emit(['setMapActive']);
              }

              return setTimeout((): void => this.eventEmitter.emit(['loadMap']));
            }

            if (evt.url === '/heatmap') {
              if (this.state.map.active) {
                this.eventEmitter.emit(['setMapActive']);
              }

              if (!this.state.heatmap.active) {
                this.eventEmitter.emit(['setHeatmapActive']);
              }

              return setTimeout((): void => this.eventEmitter.emit(['loadHeatmap']));
            }
          }

          return this.eventEmitter.emit(['logout']);
        },
        error: (err: Error): void => {
          console.error('setRoutes Failed:\n', err);
        },
        complete: (): void => {
          setRoutes.unsubscribe();
        }
      });

    const setState: Subscription = this.storeService.store$.subscribe({
      next: (state: Store): void => {
        this.state = state;
        this.heatmapService.state = state;
        this.layerUIService.state = state;
        this.mapService.state = state;
      },
      error: (err: Error): void => {
        console.error('setState Failed:\n', err);
      },
      complete: (): void => {
        setState.unsubscribe();
      }
    });
  }
}
