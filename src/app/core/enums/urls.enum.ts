export enum Urls {
  GEOJSON_ENDPOINT = '/api/geojson',
  MAPBOX_ACCESS_TOKEN_ENDPOINT = '/api/mapbox-access-token',

  LOGIN_ENDPOINT = '/auth/login',
  REGISTER_ENDPOINT = '/auth/register'
}
