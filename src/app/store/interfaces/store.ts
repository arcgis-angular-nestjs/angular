import { LayerElement } from '@map/interfaces/layer-element';
import { Trail } from '@map/interfaces/trail';

export interface Store {
  graphicsLayers: any;
  heatmap: any;
  layerElements: LayerElement[];
  map: any;
  splashScreen: any;
  trails: Trail[];
  view: any;
}
