import { Component, Input, OnInit } from '@angular/core';

import { AuthenticationService } from '@access/authentication/services/authentication.service';

@Component({
  selector: 'app-authentication',
  styleUrls: ['authentication.component.scss'],
  templateUrl: 'authentication.component.html'
})
export class AuthenticationComponent implements OnInit {
  @Input() username: string;
  @Input() password: string;

  constructor(private authenticationService: AuthenticationService) {
    this.username = 'johncampbell@geospatialweb.ca';
    this.password = 'Click_Login';
  }

  ngOnInit() {}

  onSubmit(): void {
    /* this.authenticationService.register({ */
    this.authenticationService.login({
      username: this.username,
      password: this.password
    });
  }
}
