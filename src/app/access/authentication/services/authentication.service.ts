import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { Urls } from '@core/enums/urls.enum';

import { JwtAccessToken } from '@access/authentication/interfaces/jwt-access-token';
import { User } from '@access/authentication/interfaces/user';

import { AppService } from '@app/services/app.service';
import { AuthorizationService } from '@access/authorization/services/authorization.service';
import { HttpService } from '@core/services/http/http.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(
    private appService: AppService,
    private authorizationService: AuthorizationService,
    private httpService: HttpService
  ) {}

  async login(user: User): Promise<void> {
    await this.httpService
      .post(Urls.LOGIN_ENDPOINT, user)
      .then((token: JwtAccessToken): void => {
        this.authorizationService.setSession(token);
        this.appService.loadApp();
      })
      .catch((err: HttpErrorResponse): void => {
        console.error('Login Failed:\n', err);
      });
  }

  async register(user: User): Promise<void> {
    await this.httpService
      .post(Urls.REGISTER_ENDPOINT, user)
      .then((user: User): void => {
        this.login({ username: user.username, password: user.password });
      })
      .catch((err: HttpErrorResponse): void => {
        console.error('Registration Failed:\n', err);
      });
  }
}
