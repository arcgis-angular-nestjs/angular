import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import mapboxgl from 'mapbox-gl';
import moment from 'moment/moment';

import { Urls } from '@core/enums/urls.enum';

import { JwtAccessToken } from '@access/authentication/interfaces/jwt-access-token';

import { HttpService } from '@core/services/http/http.service';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  constructor(private httpService: HttpService) {}

  async getMapboxAccessToken(): Promise<void> {
    await this.httpService
      .get(Urls.MAPBOX_ACCESS_TOKEN_ENDPOINT)
      .then((accessToken: string): void => {
        this.setMapboxAccessToken(accessToken);
      })
      .catch((err: HttpErrorResponse): void => {
        console.error('getMapboxAccessToken Failed:\n', err);
      });
  }

  private setMapboxAccessToken(accessToken: string): void {
    mapboxgl.accessToken = accessToken;
  }

  private getSessionExpiration(): moment.Moment {
    const expiresAt: number = JSON.parse(sessionStorage.getItem('expiresAt'));
    return moment(expiresAt);
  }

  hasSessionExpired(): boolean {
    return !moment().isBefore(this.getSessionExpiration());
  }

  setSession(token: JwtAccessToken): void {
    const expiresAt: moment.Moment = moment().add(token.expiresIn, 'second');

    sessionStorage.setItem('accessToken', token.accessToken);
    sessionStorage.setItem('expiresAt', JSON.stringify(expiresAt.valueOf()));
  }

  unsetSession(): void {
    sessionStorage.removeItem('accessToken');
    sessionStorage.removeItem('expiresAt');
    location.reload();
  }
}
