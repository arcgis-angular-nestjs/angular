export interface Layer {
  fields: string;
  id: string;
}
