import { EventEmitter, Injectable } from '@angular/core';
import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import * as fetch from 'd3-fetch';
import { FeatureCollection } from 'geojson';

import { Layer } from '@data/interfaces/layer';
import { Urls } from '@core/enums/urls.enum';

import { EventEmitterService } from '@core/services/eventEmitter/event-emitter.service';
import { HttpService } from '@core/services/http/http.service';

import { layers } from '@data/data/layer.data';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private eventEmitter: EventEmitter<any>;
  private layers: any[];

  constructor(private eventEmitterService: EventEmitterService, private httpService: HttpService) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
    this.layers = layers;
  }

  loadData(): void {
    this.getMapData();
    this.getHeatmapData();
  }

  private async http(params: HttpParams): Promise<FeatureCollection> {
    return await this.httpService.get(Urls.GEOJSON_ENDPOINT, { params });
  }

  private getMapData(): void {
    this.layers.forEach((layer: Layer): void => {
      const params: HttpParams = new HttpParams().set('fields', layer.fields).set('table', layer.id);

      this.http(params)
        .then((fc: FeatureCollection): void => {
          fc.features.length
            ? this.eventEmitter.emit(['createGraphicsLayer', fc, layer.id])
            : console.log(`No ${layer.id.toUpperCase()} GraphicsLayer Found:\n`, fc);
        })
        .catch((err: HttpErrorResponse): void => {
          console.error('http Failed:\n', err);
        });
    });
  }

  private getHeatmapData(): void {
    fetch
      .csv('https://raw.githubusercontent.com/uber-common/deck.gl-data/master/examples/3d-heatmap/heatmap-data.csv')
      .then((data: any[]): void => {
        if (data.length) {
          return this.eventEmitter.emit(['setHeatmapData', data]);
        }

        console.error('Data Error:\n', data);
      })
      .catch((err: Error): void => {
        console.error('getHeatmapData Failed:\n', err);
      });
  }
}
