import { async, TestBed } from '@angular/core/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

import { AppComponent } from './app.component';
import { HeaderComponent } from '@app/components/header/header.component';
import { SplashScreenComponent } from '@app/components/splashScreen/splash-screen.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent, HeaderComponent, SplashScreenComponent],
      imports: [RouterTestingModule],
      providers: [HttpClient, HttpHandler]
    }).compileComponents();
  }));

  it('should create app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Geospatial Web'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('Geospatial Web');
  });
});
