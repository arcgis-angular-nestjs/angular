import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconUIComponent } from './icon-ui.component';

describe('IconUIComponent', () => {
  let component: IconUIComponent;
  let fixture: ComponentFixture<IconUIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IconUIComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => expect(component).toBeTruthy());
});
