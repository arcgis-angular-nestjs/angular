import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';

import { StoreService } from '@store/services/store.service';

@Component({
  selector: 'app-trail-ui',
  styleUrls: ['trail-ui.component.scss'],
  templateUrl: 'trail-ui.component.html',
  animations: [
    trigger('slide', [
      state(
        'slide-out',
        style({
          transform: 'translateX(-150px)'
        })
      ),
      state(
        'slide-in',
        style({
          transform: 'translateX(0)'
        })
      ),
      transition('slide-out => slide-in', animate(1000))
    ])
  ]
})
export class TrailUIComponent implements OnInit {
  slide = 'slide-out';

  constructor(public storeService: StoreService) {}

  ngOnInit(): void {
    setTimeout((): string => (this.slide = 'slide-in'));
  }
}
