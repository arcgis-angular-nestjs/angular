import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayerUIComponent } from './layer-ui.component';

describe('LayerUIComponent', () => {
  let component: LayerUIComponent;
  let fixture: ComponentFixture<LayerUIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LayerUIComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayerUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => expect(component).toBeTruthy());
});
