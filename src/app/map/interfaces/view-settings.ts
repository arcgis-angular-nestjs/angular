import esri = __esri;

export interface ViewSettings {
  center: esri.Point | null;
  extent: esri.Extent | null;
  scale: number | null;
  zoom: number | null;
}
