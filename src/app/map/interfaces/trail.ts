import esri = __esri;

export interface Trail {
  active?: boolean;
  center: esri.Point;
  name?: string;
  zoom?: number;
}
