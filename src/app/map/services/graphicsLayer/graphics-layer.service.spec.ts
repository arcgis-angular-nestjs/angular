import { TestBed } from '@angular/core/testing';

import { GraphicsLayerService } from './graphics-layer.service';

describe('GraphicsLayerService', () => {
  let service: GraphicsLayerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GraphicsLayerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
