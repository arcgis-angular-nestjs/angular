import { Injectable } from '@angular/core';
import Graphic from 'arcgis-js-api/Graphic';
import GraphicsLayer from 'arcgis-js-api/layers/GraphicsLayer';
import PopupTemplate from 'arcgis-js-api/PopupTemplate';
import { FeatureCollection } from 'geojson';

@Injectable({
  providedIn: 'root'
})
export class GraphicsLayerService {
  graphicsLayers: GraphicsLayer[];
  graphicsLayersHash: {};

  constructor() {
    this.graphicsLayers = [];
    this.graphicsLayersHash = {};
  }

  private createGraphic(attributes: {}, geometry: {}, symbol: {}, popupTemplate?: PopupTemplate): Graphic {
    return new Graphic({
      attributes,
      geometry,
      popupTemplate,
      symbol
    });
  }

  private createPointGraphicsLayer(fc: FeatureCollection, id: string): GraphicsLayer {
    const gl: GraphicsLayer = new GraphicsLayer();
    gl.geometryType = 'point';
    gl.visible = false;

    fc.features.forEach((feature: any) => {
      const attributes: {} = feature.properties;

      const geometry: {} = {
        type: 'point',
        longitude: feature.properties.lng || feature.geometry.coordinates[0],
        latitude: feature.properties.lat || feature.geometry.coordinates[1]
      };

      const symbol: {} = {
        type: 'picture-marker',
        url: `assets/${id}.png`,
        height: '25px',
        width: '22px'
      };

      gl.add(this.createGraphic(attributes, geometry, symbol));
    });

    return gl;
  }

  private createPolygonGraphicsLayer(fc: FeatureCollection, id: string): GraphicsLayer {
    const gl: GraphicsLayer = new GraphicsLayer();
    gl.geometryType = 'polygon';

    id === 'biosphere' ? (gl.visible = true) : (gl.visible = false);

    fc.features.forEach((feature: any) => {
      const attributes: {} = {};

      const geometry: {} = {
        type: 'polygon',
        rings: feature.geometry.coordinates
      };

      const symbol: {} = {
        type: 'simple-fill',
        color: 'rgba(0, 153, 0, 0.3)',
        style: 'solid',
        outline: {
          color: 'rgb(0, 0, 0)',
          width: '1px'
        }
      };

      const popupTemplate: PopupTemplate = new PopupTemplate({
        title: feature.properties.name,
        content: feature.properties.description
      });

      gl.add(this.createGraphic(attributes, geometry, symbol, popupTemplate));
    });

    return gl;
  }

  private createPolylineGraphicsLayer(fc: FeatureCollection): GraphicsLayer {
    const gl: GraphicsLayer = new GraphicsLayer();
    gl.geometryType = 'polyline';
    gl.visible = false;

    fc.features.forEach((feature: any) => {
      const attributes: {} = {};

      const geometry: {} = {
        type: 'polyline',
        paths: feature.geometry.coordinates
      };

      const symbol: {} = {
        type: 'simple-line',
        color: 'rgb(153, 0, 0)',
        style: 'solid',
        width: '2px'
      };

      gl.add(this.createGraphic(attributes, geometry, symbol));
    });

    return gl;
  }

  private createGraphicsLayersArray(gl: GraphicsLayer, id: string): void {
    this.graphicsLayers.push(gl);
    this.graphicsLayersHash[id] = this.graphicsLayers.length - 1;
  }

  createGraphicsLayer(fc: FeatureCollection, id: string): void {
    switch (fc.features[0].geometry.type) {
      case 'LineString':
        this.createGraphicsLayersArray(this.createPolylineGraphicsLayer(fc), id);
        this.createGraphicsLayersArray(this.createPointGraphicsLayer(fc, id), `${id}-point`);
        break;

      case 'Point':
        this.createGraphicsLayersArray(this.createPointGraphicsLayer(fc, id), id);
        break;

      case 'Polygon':
        this.createGraphicsLayersArray(this.createPolygonGraphicsLayer(fc, id), id);
        break;
    }
  }

  setGraphicsLayerVisible(id: string): void {
    this.graphicsLayers[this.graphicsLayersHash[id]].visible = !this.graphicsLayers[this.graphicsLayersHash[id]]
      .visible;
  }
}
