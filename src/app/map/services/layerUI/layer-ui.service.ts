import { EventEmitter, Injectable } from '@angular/core';

import { Store } from '@store/interfaces/store';

import { EventEmitterService } from '@core/services/eventEmitter/event-emitter.service';

@Injectable({
  providedIn: 'root'
})
export class LayerUIService {
  private eventEmitter: EventEmitter<any>;

  state: Store;

  constructor(private eventEmitterService: EventEmitterService) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
  }

  selectLayer(id: string): void {
    switch (id) {
      case 'biosphere':
      case 'office':
      case 'places':
      case 'trails':
      case 'trails-point':
        this.eventEmitter.emit(['setGraphicsLayerActive', id]);

        this.state.graphicsLayers[id].active
          ? this.eventEmitter.emit(['addGraphicsLayer', id])
          : this.eventEmitter.emit(['removeGraphicsLayer', id]);
        break;

      case 'heatmap':
        this.eventEmitter.emit(['setRoutePath', '/heatmap']);
        break;

      case 'logout':
        this.eventEmitter.emit(['logout']);
        break;
    }
  }
}
