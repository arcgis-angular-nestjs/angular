import { EventEmitter, Injectable } from '@angular/core';
import BasemapToggle from 'arcgis-js-api/widgets/BasemapToggle';
import GraphicsLayer from 'arcgis-js-api/layers/GraphicsLayer';
import Map from 'arcgis-js-api/Map';
import MapView from 'arcgis-js-api/views/MapView';

import { Store } from '@store/interfaces/store';
import { Trail } from '@map/interfaces/trail';
import { ViewSettings } from '@map/interfaces/view-settings';

import { EventEmitterService } from '@core/services/eventEmitter/event-emitter.service';

@Injectable({
  providedIn: 'root'
})
export class MapService {
  private basemapToggle: BasemapToggle;
  private eventEmitter: EventEmitter<any>;
  private map: Map;
  private view: MapView;
  private viewSettings: ViewSettings;

  state: Store;

  constructor(private eventEmitterService: EventEmitterService) {
    this.eventEmitter = this.eventEmitterService.eventEmitter;
    this.viewSettings = {
      center: null,
      extent: null,
      scale: null,
      zoom: null
    };
  }

  loadMap(): void {
    this.map = new Map({
      basemap: this.state.map.basemap.active
    });

    this.view = new MapView({
      center: this.state.view.settings.center,
      container: this.state.view.settings.container,
      map: this.map,
      popup: {
        dockOptions: {
          buttonEnabled: this.state.view.popup.dockOptions.buttonEnabled
        }
      },
      zoom: this.state.view.settings.zoom
    });

    this.view.constraints = {
      maxZoom: this.state.view.constraints.maxZoom,
      minZoom: this.state.view.constraints.minZoom
    };

    this.setBasemapToggle();

    this.view.when((): void => {
      this.view.ui.add(this.basemapToggle, 'top-right');
      this.view.watch('extent', (): void => this.setViewSettings());

      this.view.on('pointer-move', (evt: PointerEvent) => {
        this.view.hitTest(evt).then((res: { results: any[] }) => {
          if (res.results.length && res.results[0].graphic.geometry.type === 'point') {
            return this.view.popup.open({
              location: res.results[0].mapPoint,
              title: res.results[0].graphic.attributes.name,
              content: res.results[0].graphic.attributes.description
            });
          }

          if (this.view.popup.visible) {
            return this.view.popup.close();
          }

          return true;
        });
      });

      this.eventEmitter.emit(['addGraphicsLayers']);
    });
  }

  private setBasemapToggle(): void {
    this.basemapToggle = new BasemapToggle({
      view: this.view,
      nextBasemap: this.state.map.basemap.next
    });

    this.basemapToggle.on('toggle', (): void => {
      this.eventEmitter.emit(['setBasemapActive']);
    });
  }

  private setViewSettings(): void {
    this.viewSettings.center = this.view.center;
    this.viewSettings.extent = this.view.extent;
    this.viewSettings.scale = this.view.scale;
    this.viewSettings.zoom = this.view.zoom;
    this.eventEmitter.emit(['setViewSettings', this.viewSettings]);
  }

  addGraphicsLayer(gl: GraphicsLayer): void {
    switch (gl.geometryType) {
      case 'polygon':
        this.map.add(gl, 0);
        break;

      case 'polyline':
        this.map.add(gl, 1);
        break;

      case 'point':
        this.map.add(gl, 2);
        break;
    }
  }

  removeGraphicsLayer(gl: GraphicsLayer): void {
    this.map.remove(gl);
  }

  goTo(trail: Trail): void {
    this.view.goTo({
      center: trail.center,
      zoom: trail.zoom
    });
  }
}
