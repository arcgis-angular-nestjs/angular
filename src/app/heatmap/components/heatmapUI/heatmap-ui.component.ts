import { Component, OnInit } from '@angular/core';

import { StoreService } from '@store/services/store.service';

@Component({
  selector: 'app-heatmap-ui',
  styleUrls: ['heatmap-ui.component.scss'],
  templateUrl: 'heatmap-ui.component.html'
})
export class HeatmapUIComponent implements OnInit {
  constructor(public storeService: StoreService) {}

  ngOnInit() {}
}
