import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeatmapUIComponent } from './heatmap-ui.component';
import { HexagonUIComponent } from '../hexagonUI//hexagon-ui.component';

describe('HeatmapUIComponent', () => {
  let component: HeatmapUIComponent;
  let fixture: ComponentFixture<HeatmapUIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeatmapUIComponent, HexagonUIComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeatmapUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => expect(component).toBeTruthy());
});
