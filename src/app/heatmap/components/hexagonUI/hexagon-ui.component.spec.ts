import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HexagonUIComponent } from './hexagon-ui.component';

describe('HexagonUIComponent', () => {
  let component: HexagonUIComponent;
  let fixture: ComponentFixture<HexagonUIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HexagonUIComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HexagonUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => expect(component).toBeTruthy());
});
