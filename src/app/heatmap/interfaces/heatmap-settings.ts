export interface HeatmapSettings {
  bearing: number | null;
  center: number[] | null;
  pitch: number | null;
  zoom: number | null;
}
